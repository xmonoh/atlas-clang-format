FROM debian:sid-slim

RUN apt-get update && \
    apt-get install -y clang-format-6.0 && \
    apt-get install -y openssh-client && \
    apt-get install -y git

RUN ln -s /bin/clang-format-6.0 /bin/clang-format

COPY scripts /scripts/
COPY style/clang-format.style /.clang-format

# RUN useradd -c 'clang-format-bot' -m -d /home/clang -s /bin/bash clang
# USER clang
# ENV HOME /home/clang

CMD bash