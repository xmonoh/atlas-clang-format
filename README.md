## atlas-clang-format

ClangFormat describes a set of tools that are built on top of LibFormat. It can support your workflow in a variety of ways including a standalone tool and editor integrations.

This project makes avaliable a docker container to force good code formatting on the user which helps to maintain readable code.